This is a Python module aimed at running molecular dynamics (MD) simulations with LAMMPS. It provides the user with an API allowing to build simple and flexible applications.

## Issue

When doing MS or MD simulations, new users of LAMMPS may find hard to setup an efficient workflow, especially because :

- LAMMPS is designed mostly as a stand-alone program, that you have to feed with input files. Writing them all by hand is slow and error prone. LAMMPS can also be imported as a Python library or used as an external server, but it raises the same issues in the end.
- Tracking the conditions in which a result was obtained is essential for reproductibility. Using LAMMPS can result in facing a number of files that seem similar, with no clear hierarchy or timeline.
- Extracting data from LAMMPS simulations results is cumbersome, as they are all concatenated in log files.
- Lammps usually throws cryptic error messages, or passes errors silently. One needs to make sure his simulation works in a set of reference cases.

## Features

- Prepare systems containing perfect crystal, dislocations dipoles, with or without C atoms, using ASE, Atomman, Babel, Atomsk.

With the tools provided in the API, you'll be able to write applications to :


## Requirements

- ASE
- Atomman
- Atomsk