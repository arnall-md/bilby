from __future__ import division, absolute_import, print_function
import os
import math
import datetime
import numpy as np
import pathlib
import subprocess
import tempfile
import random
from copy import deepcopy

import atomman as am  # https://github.com/usnistgov/atomman
import atomman.lammps as lmp
import atomman.unitconvert as uc
# import iprPy  # https://github.com/usnistgov/iprPy
# from DataModelDict import DataModelDict as DM  # https://github.com/usnistgov/DataModelDict

from . import generate


class PADbox:
    """ A class that represents a Periodic Array of Dislocations (PAD) simulation box.
    """

    def __init__(self, lattice, Nx, Ny, Nz):
        """Class initializer.

        Parameters
        ----------

        lattice : float
            The target lattice parameter of the box to create.
        Nx : int
            Number of primitive cells along `x` axis.
        Ny : int
            Number of primitive cells along `y` axis.
        Ny : int
            Number of primitive cells along `z` axis.

        """
        self.lattice = lattice
        self.dimensions = [Nx, Ny, Nz]
        self.burgers = lattice*math.sqrt(3)/2
        self.system, self.bulk = PADbox.Create_PAD(self, lattice, Nx, Ny, Nz)

    def Create_PAD(self, lattice, Nx, Ny, Nz):
        """Creates PAD(Periodic Array of Dislocations) simulation box in BCC iron[1]_.
        Its orientation is x = [111], y = [-1-12], z = [1-10].
        It uses the command line tool Atomsk by Pierre Hiriel[2]_.
        It is NOT RELAXED, and naively created with respect to the
        lattice parameter passed as parameter.

        Parameters
        ----------

        lattice : float
            The target lattice parameter of the box to create.
        Nx : int
            Number of primitive cells along `x` axis.
        Ny : int
            Number of primitive cells along `y` axis.
        Ny : int
            Number of primitive cells along `z` axis.

        Returns
        ----------

        system : atomman.System
            The created PAD simulation box.
        bulk : atomman.System
            The same system, with no dislocation. Useful for drawing differential displacement maps.

        References
        ----------
        .. [1] Bacon, D. J., Osetsky, Y. N., & Rodney, D. (2009). Chapter 88 Dislocation-Obstacle Interactions at the Atomic Level. Dislocations in Solids (Vol. 15). https://doi.org/10.1016/S1572-4859(09)01501-0
        .. [2] "Atomsk: A tool for manipulating and converting atomic data files"
        Pierre Hirel, Comput. Phys. Comm. 197 (2015) 212-219 | doi: 10.1016/j.cpc.2015.07.012
        """

        with tempfile.TemporaryDirectory() as tmp:
            tmp_path = pathlib.Path(tmp).joinpath("PAD.lmp")

            atomsk_command = """atomsk --create bcc {lattice} Fe orient [111] [-1-12] [1-10] -duplicate {Nx} {Ny} {Nz} {tmp}""".format(
                lattice=lattice,
                Nx=Nx,
                Ny=Ny,
                Nz=Nz,
                tmp=tmp_path)
            # System call to Atomsk
            subprocess.run(
                atomsk_command,
                shell=True,
                capture_output=True,
                check=True,
                timeout=2,
                universal_newlines=True)
            # Load system in atomman
            system = am.load('atom_data', str(tmp_path))

        initial_box = np.asarray(system.box.vects)  # copy box vectors
        Lx, Ly, Lz = system.box.a, system.box.b, system.box.c  # get lattice
        # burgers = lattice*math.sqrt(3)/2  # get burgers

        bulk = deepcopy(system)
        bulk.box.set(vects=[initial_box[0],
                            initial_box[1],  # no tilt
                            initial_box[2] + [0, 0, Lz]])  # add vaccum

        system.box.set(vects=[initial_box[0],
                              initial_box[1] + [self.burgers/2, 0, 0],  # tilt yz plan
                              initial_box[2] + [0, 0, Lz]])  # add vaccum

        y_dislo = Ly/2-self.burgers/2 if Ny % 2 == Nz % 2 else Ly / \
            2  # position of the dislocation core
        z_dislo = Lz/2
        # displacement field of dislocation
        system.atoms.pos = [[p[0] + self.burgers/2/np.pi*math.atan2(p[2]-z_dislo, p[1]-y_dislo),
                            p[1],
                            p[2]] for p in system.atoms.pos]
        return system, bulk

    def sys_in_format(self, out_format):
        """Returns the `system` attribute in desired format(s). See bilby.generate.return_management"""
        return generate.return_management(self.system, out_format)

    def bulk_in_format(self, out_format):
        """Returns the `bulk` attribute in desired format(s). See bilby.generate.return_management"""
        return generate.return_management(self.bulk, out_format)

    def add_solid_solution(self, natoms_to_add, n_empty_rows=0, buffer=0.8):
        """Adds C atoms randomly in a PAD box.

        Parameters
        ----------

        natoms_to_add : float
            The target lattice parameter of the box to create. If n_empty_rows is used, a part of those atoms will be removed, and the returned
            box will have less atoms than `n_atoms_to_add`.
        n_empty_rows : int (optional)
            Number of rows left without carbon, from the center of the box. Default is 0. 
        buffer : float (optional)
            Fraction of the box to be filled with carbon, in order to leave the free surfaces carbon-free. This fraction doesn't account for the 
            effect of n_empty_rows.

        """
        if natoms_to_add != 0:
            lz_crystal = self.system.box.cvect[2]/2
            interrow_distance = self.lattice / np.sqrt(2)
            cutoff = n_empty_rows * interrow_distance

            # z_center_crystal = middle of the crystal minus 1/2 atoms row
            z0 = lz_crystal/2 - interrow_distance/2

            random.seed(123)
            atoms_to_add = np.asarray([x + PADbox.pick_random_position(self.lattice)
                                    for x in random.sample(list(self.system.atoms.pos[buffer*lz_crystal/2 > abs(self.system.atoms.pos[:, 2] - z0)]), natoms_to_add)])

            # Restrict box to area of interest
            atoms_in_interest_zone = atoms_to_add[abs(
                atoms_to_add[:, 2] - z0) >= cutoff]
            self.system = PADbox.add_C_atom(self.system, atoms_in_interest_zone)

    def add_C_atom(system, pos_, symbol='C'):
        """Adds one or more atoms at position(s) pos = [[x1,y1,z1], [x2,y2,z2],...] to a system that contains pure iron.
        """
        system.symbols = ("Fe",)
        atoms_to_add = am.Atoms(atype=2, pos=pos_)
        return system.atoms_extend(atoms_to_add, symbols=system.symbols+(symbol,))

    def pick_random_position(lattice):
        """Considering an atom, a random octahedral position will be given relatively to this atom. 
        Only depends on lattice parameter.
        """
        d = lattice*np.sqrt(6)/3 # the projected distance between two atoms in yz plan
        b = 0.5*lattice*np.sqrt(3)  # b = 1/2 [111] = 1/2 * a0 * sqrt(3)

        z = np.sqrt(3)*d/4
        # 6 possible octahedral positions around an atom in BCC structure
        random.seed(123)
        position = random.choice([
            [b/3, d/2, 0],
            [-b/3, -d/2, 0],
            [-b/6, -d/4, z],
            [-b/6, -d/4, -z],
            [b/6, d/4, z],
            [b/6, d/4, -z]
        ])
        return position
