# Freely adaptated from iprPy relax_static example notebook by Arnaud Allera
#   Credits : Lucas M. Hale, Chandler A. Becker, Zachary T. Trautt -- NIST
#   In Version: 2018-06-24

from __future__ import division, absolute_import, print_function
import os
import sys
import datetime
import numpy as np
from pathlib import Path
import re
import shutil

import atomman as am  # https://github.com/usnistgov/atomman
import atomman.lammps as lmp
import atomman.unitconvert as uc
# import iprPy  # https://github.com/usnistgov/iprPy
import ase
from ase import io

def relax_static(
        lammps_command, system, potential, template, workdir, format_output,
        box_relax=False, mpi_command=None, system_info=None, write_data='',
        pressures_ij=None, dispmult=0.0, etol=0.0, ftol=0.0,  maxiter=10000,
        maxeval=100000, dmax=uc.set_in_units(0.01, 'angstrom'), maxcycles=100,
        ctol=1e-10, timestep=1e-2, min_style='cg'):
    """
    Repeatedly runs the template script, inspired from ELASTIC example
    distributed with LAMMPS, until system energy converges within a tolerance.

    Parameters
    ----------
    lammps_command : str
        Command for running LAMMPS.
    system : atomman.System
        The system to perform the calculation on.
    potential : atomman.lammps.Potential
        The LAMMPS implemented potential to use.
    template : pathlib.Path object
        Path to the template to be used.
    workdir : Pathlib object
        Location to put files in.
    box_relax : bool, optional
        States if the `fix box relax` Lammps command should be used. It allows
        to dynamically adapt the dimensions and tilt of the simulation box
        in order to minimize forces (e.g. to accomodate an eigenstrain).
    mpi_command : str, optional
        The MPI command for running LAMMPS in parallel.  If not given, LAMMPS
        will run serially.
    system_info : str, optional
        Lammps commands to use for filling in the "system info" section
        of the template (default is None).
    write_data : pathlib Path object, optional
        Location where to target Lammps `write_data` at. Default is don't use
        `write_data`. Written file can be open by Lammps `read_data` command.
    pressires_ij : dict, optional
        The values of pxx, pyy, ... to relax the tensile pressure component
        to (default is all 0.0).
    dispmult : float, optional
        Multiplier for applying a random displacement to all atomic positions
        prior to relaxing. Default value is 0.0.
    etol : float, optional
        The energy tolerance for the structure minimization. This value is
        unitless. (Default is 0.0).
    ftol : float, optional
        The force tolerance for the structure minimization. This value is in
        units of force. (Default is 0.0).
    maxiter : int, optional
        The maximum number of minimization iterations to use (default is 10000).
    maxeval : int, optional
        The maximum number of minimization evaluations to use (default is 
        100000).
    dmax : float, optional
        The maximum distance in length units that any atom is allowed to relax
        in any direction during a single minimization iteration (default is
        0.01 Angstroms).
    pressure_unit : str, optional
        The unit of pressure to calculate the elastic constants in (default is
        'GPa').
    maxcycles : int, optional
        The maximum number of times the minimization algorithm is called.
        Default value is 100. Used only if `box_relax = True`
    ctol : float, optional
        The relative tolerance used to determine if the lattice constants have
        converged (default is 1e-10). Used only if `box_relax = True`
    timestep : float, optional
        The timestep required by some minimizers.
    min_style : str, optional
        The minimization algorithm to use for relaxing. Note from Lammps
        documentation : *The quickmin, fire, and hftn styles do not yet
        support the use of the fix box/relax command or minimizations
        involving the electron radius in eFF models.*

    Returns
    -------
    dict
        Dictionary of results consisting of keys:
        
        - **dumpfile_initial** (*float*) - Path to a dumpfile BEFORE starting lammps.
        - **dumpfile_final** (*float*) - Path to a dumpfile AFTER starting lammps.
        - **relaxed_system** (*float*) - The relaxed system.
        - **E_coh** (*float*) - The cohesive energy of the relaxed system.
        - **E_coh_per_at** (*float*) - The cohesive energy of the relaxed system divided
          by the number of atoms.
        - **lx, ly, lz, xy, yz, xz** (*float*) - The dimensions of the relaxed simulation
          box.
        - **measured_pxx** (*float*) - The measured x tensile pressure of the
          relaxed system.
        - **measured_pyy** (*float*) - The measured y tensile pressure of the
          relaxed system.
        - **measured_pzz** (*float*) - The measured z tensile pressure of the
          relaxed system.
        - **measured_pxy** (*float*) - The measured xy shear pressure of the
          relaxed system.
        - **measured_pxz** (*float*) - The measured xz shear pressure of the
          relaxed system.
        - **measured_pyz** (*float*) - The measured yz shear pressure of the
          relaxed system.
        - **Stopping_Criterion** (*str*) - The minimization Stopping criterion 
          read from lammps logfile.
        - **success**: True if 'force tolerence' in stopping_criter else False
    """

    # Get lammps units
    lammps_units = lmp.style.unit(potential.units)

    # Get lammps version date
    lammps_date = lmp.checkversion(lammps_command)['date']
    workdir.mkdir(parents=True, exist_ok=True)


    # Save initial configuration as a dump file
    system.dump('atom_dump', f=workdir.joinpath('initial.dump'))

    # Apply small random distortions to atoms
    system.atoms.pos += dispmult * \
        np.random.rand(*system.atoms.pos.shape) - dispmult / 2

    if box_relax:
        # Initialize parameters
        old_vects = system.box.vects
        converged = False
    else :
        maxcycles = 1

    # Run minimizations up to maxcycles times
    for cycle in range(maxcycles):
        # old_system = deepcopy(system) # never used - to be removed

        # Define a "lammps variables" dict
        lammps_variables = {}
        if system_info is None:
            system_info = system.dump('atom_data', f=str(workdir.joinpath('init.dat')),
                                      units=potential.units,
                                      atom_style=potential.atom_style)
        lammps_variables = {
            'system_info': system_info,
            'atomman_pair_info': potential.pair_info(system.symbols),
            'etol': etol,
            'ftol': uc.get_in_units(ftol, lammps_units['force']),
            'maxiter': maxiter,
            'maxeval': maxeval,
            'dmax': uc.get_in_units(dmax, lammps_units['length'],),
            'timestep': timestep,
            'min_style': min_style
        }
        if pressures_ij is not None:
            pressures_ij = {key: uc.get_in_units(val, lammps_units['pressure']) for key, val in pressures_ij.items()}
        else:
            pressures_ij = {key: 0.0 for key in ('p_xx', 'p_yy', 'p_zz', 'p_xy', 'p_xz', 'p_yz')}
        lammps_variables.update(pressures_ij)

        if box_relax:
            lammps_variables['box_relax'] = ""
        else:
            lammps_variables['box_relax'] = """#"""

        if write_data != '':
            lammps_variables['write_data'] = "write_data {}".format(
                str(write_data))
        else:
            lammps_variables['write_data'] = ""

        # Set dump_modify_format based on lammps_date, for version compatibility
        if lammps_date < datetime.date(2016, 8, 3):
            print("Info : an old Lammps version is used (older than 2016, 08, 3rd).")
            lammps_variables['dump_modify_format'] = '"%d %d %.13e %.13e %.13e %.13e"'
        else:
            lammps_variables['dump_modify_format'] = 'float %.13e'

        # Write lammps input script
        template_file = template.with_suffix('.template')
        lammps_script = workdir.joinpath(template.stem).with_suffix('.in')

        with open(template_file, 'r') as f:
            open_template = f.read()
        with open(lammps_script, 'w') as f:
            f.write(iprPy.tools.filltemplate(
                open_template, lammps_variables, '<', '>'))

        # Run LAMMPS and extract thermo data
        logfile = workdir.joinpath('log-{}.lammps'.format(cycle))
        output = lmp.run(lammps_command, str(lammps_script),
                         mpi_command, logfile=str(logfile))
        thermo = output.simulations[0]['thermo']

        # Clean up dump files
        os.remove(Path.cwd().joinpath('0.dump'))
        last_dump_file = str(thermo.Step.values[-1]) + '.dump'
        renamed_dump_file = workdir.joinpath('final.dump')
        shutil.move(last_dump_file, renamed_dump_file)
        last_dump_file = renamed_dump_file
        
        try:
            with open(last_dump_file, 'r'):
                system = am.load('atom_dump', last_dump_file,
                        symbols=system.symbols)  # toggles an error if lammps has failed
                # Test if box dimensions have converged
                if box_relax and np.allclose(old_vects, system.box.vects, rtol=ctol, atol=0):
                    converged = True
                    break
                else:
                    old_vects = system.box.vects
        except:
            print('Minimization failed :\n{}\n{}\n{}'.format(potential, workdir, template))

    # Output dict :
    final_system = {}
    if 'atomman' in format_output:
        final_system['atomman'] = am.load('atom_dump', last_dump_file,
                        symbols=system.symbols)
    if 'ase' in format_output:
        with open(last_dump_file, 'r') as f:
            final_system['ase'] = ase.io.read(f, format='lammps-dump')
    if 'dump' in format_output:
        with open(last_dump_file, 'r') as dumpfile:
            final_system['dump'] = dumpfile.read()
    if 'lammps' in format_output:
        with open(write_data, 'r') as lmpfile:
            final_system['lammps'] = lmpfile.read()

    if box_relax:
        # Check for convergence
        if converged is False and maxcycles != 1:
            raise RuntimeError('Failed to converge after ' +
                            str(maxcycles) + ' cycles')

    with open(logfile, 'r') as logs:
        stopping_criter = re.findall("Stopping criterion = ([\w\s]+)\n", logs.read())
    lx = system.box.lx
    ly = system.box.ly
    lz = system.box.lz
    xy = system.box.xy
    xz = system.box.xz
    yz = system.box.yz
    # Zero out near-zero tilt factors
    # Eg. replace 1E-10 by 0.0
    # Not useful for me
    # if np.isclose(xy/ly, 0.0, rtol=0.0, atol=1e-10):
    #     xy = 0.0
    # if np.isclose(xz/lz, 0.0, rtol=0.0, atol=1e-10):
    #     xz = 0.0
    # if np.isclose(yz/lz, 0.0, rtol=0.0, atol=1e-10):
    #     yz = 0.0
    # system.box.set(lx=lx, ly=ly, lz=lz, xy=xy, xz=xz, yz=yz)

    # Build results_dict
    results_dict = {
        'dumpfile_initial': 'initial.dump',
        'dumpfile_final': str(last_dump_file),
        'E_coh': uc.set_in_units(thermo.PotEng.values[-1],
                                            lammps_units['energy']),
        'E_coh_per_at': uc.set_in_units(thermo.PotEng.values[-1] / system.natoms,
                                            lammps_units['energy']),
        'lx': uc.set_in_units(lx, lammps_units['length']),
        'ly': uc.set_in_units(ly, lammps_units['length']),
        'lz': uc.set_in_units(lz, lammps_units['length']),
        'xy': uc.set_in_units(xy, lammps_units['length']),
        'xz': uc.set_in_units(xz, lammps_units['length']),
        'yz': uc.set_in_units(yz, lammps_units['length']),
        'measured_pxx': uc.set_in_units(thermo.Pxx.values[-1],
                                                   lammps_units['pressure']),
        'measured_pyy': uc.set_in_units(thermo.Pyy.values[-1],
                                                   lammps_units['pressure']),
        'measured_pzz': uc.set_in_units(thermo.Pzz.values[-1],
                                                   lammps_units['pressure']),
        'measured_pxy': uc.set_in_units(thermo.Pxy.values[-1],
                                                   lammps_units['pressure']),
        'measured_pxz': uc.set_in_units(thermo.Pxz.values[-1],
                                                   lammps_units['pressure']),
        'measured_pyz': uc.set_in_units(thermo.Pyz.values[-1],
                                                   lammps_units['pressure']),
        'Stopping_Criterion': stopping_criter[0],
        'success': True if 'force tolerance' in stopping_criter[0] else False,
        'nstep': thermo.Step.values[-1]
        }
    return results_dict, final_system
