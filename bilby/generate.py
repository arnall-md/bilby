from __future__ import division, absolute_import, print_function
import os
import math
import datetime
import numpy as np
import pathlib
import subprocess
import tempfile
from copy import deepcopy

import atomman as am  # https://github.com/usnistgov/atomman
import atomman.lammps as lmp
import atomman.unitconvert as uc
import ase
from ase import io
# import iprPy  # https://github.com/usnistgov/iprPy
# from DataModelDict import DataModelDict as DM  # https://github.com/usnistgov/DataModelDict


def dipole_bulk(lattice, n, m, out_format='atomman'):
    """Create a simulation box of pure BCC iron that complies with [1]_ page 50.
    Its vectors are such that a dislocations dipole can be introduced
    afterwards. Its orientation is x = [-1-12], y = [1-10], z = [111].
    It uses the command line tool Atomsk by Pierre Hiriel [2]_.
    It is NOT RELAXED, and naively created with respect to the
    lattice parameter passed as parameter.

    Parameters
    ----------

    lattice : float
        The target lattice parameter of the box to create.
    n : int
        Number of primitive cells horizontally (consitent with [1]_).
    m : int
        Number of primitive cells vertically (consitent with [1]_).
    out_format : str or list of str, optional
        The format(s) of the system to return. Default is `atomman`.

    Returns
    ----------
    system : atomman System object
        The generated simulation box.

    References
    ----------
    .. [1] DEZERALD, Lucile. Modélisation ab-initio des dislocations dans les
        métaux de transition cubiques centrés. 2014. Thèse de doctorat. Grenoble.
    .. [2] "Atomsk: A tool for manipulating and converting atomic data files"
        Pierre Hirel, Comput. Phys. Comm. 197 (2015) 212-219 | doi:10.1016/j.cpc.2015.07.012

    Examples
    --------
    >>> bilby.generate.create_bulk(2.855, 15, 9)
    """
    for arg in (n, m):
        if not isinstance(arg, int):
            raise TypeError("Expected int, got %s" % (type(arg),))

    atomsk_args = {
        "lattice": lattice,
        "x": math.ceil(n/3),
        "y": math.ceil(m/2),
        "cut": 1 / math.sqrt(2) * lattice * m * (1-1e-3)
    }

    temp = pathlib.Path('./tmp')
    temp.mkdir(exist_ok=True)
    try:
        temp.joinpath("atomsk_bulk.lmp").unlink()
    except:
        pass

    atomsk_bulk = subprocess.run(
        "atomsk --create bcc {lattice} Fe orient [-1-12] [1-10] [111] -duplicate {x} {y} 1 -cut above {cut} y tmp/atomsk_bulk.lmp > ./tmp/log.txt".format(**atomsk_args),
        shell=True,
        check=True,
        timeout=2,
        universal_newlines=True
        )
    system = am.load('atom_data', str(temp.joinpath('atomsk_bulk.lmp')))
    system.symbols = ['Fe']
    initial_box = np.asarray(system.box.vects)
    system.box.set(vects=[initial_box[0],
                          [initial_box[0][0]/2,
                          initial_box[1][1] - lattice/math.sqrt(2), 0],
                          initial_box[2]])
    return return_management(system, out_format)


def cubic_bulk(lattice, Nx=1, Ny=1, Nz=1, out_format="atomman"):
    """Create a cubic simulation box of pure BCC iron.
    Its orientation is x = [001], y = [010], z = [001].
    It uses the command line tool Atomsk by Pierre Hiriel [1]_.
    It is NOT RELAXED, and naively created with respect to the
    lattice parameter passed as parameter.

    Parameters
    ----------

    lattice : float
        The target lattice parameter of the box to create.
    Nx : int, optional
        Number of primitive cells along `x` axis. Default is 1.
    Ny : int, optional
        Number of primitive cells along `y` axis. Default is 1.
    Ny : int, optional
        Number of primitive cells along `z` axis. Default is 1.
    out_format : str or list of str, optional
        The format(s) of the system to return. Default is `atomman`.

    Returns
    ----------
    system : str or ase.Atoms or atomman.System
        An atomic system file that contains the generated iron bulk system, NOT RELAXED.
        If `out_format = 'lammps'`, it's formatted as a lammps-readable string.
        If `out_format = 'ase'` , an ase.Atoms object is returned.
        If `out_format = 'atomman'` , an atomman.System object is
        returned.

    References
    ----------
    .. [1] "Atomsk: A tool for manipulating and converting atomic data files"
        Pierre Hirel, Comput. Phys. Comm. 197 (2015) 212-219 | doi:10.1016/j.cpc.2015.07.012

    Examples
    --------
    >>> bilby.generate.cubic_bulk(2.855, 3, 3, 3)
    """
    with tempfile.TemporaryDirectory() as tmp:
        tmp_path = pathlib.Path(tmp).joinpath("bulk_cube.lmp")
        # add_C = """-add-atom C at {x_C} {y_C} {z_C}""".format(
        #     x_C=x_C,
        #     y_C=y_C,
        #     z_C=z_C)

        atomsk_command = """atomsk --create bcc {lattice} Fe -duplicate {Nx} {Ny} {Nz} {tmp}""".format(
            lattice=lattice,
            Nx=Nx,
            Ny=Ny,
            Nz=Nz,
            tmp=tmp_path)

        subprocess.run(
                atomsk_command,
                shell=True,
                check=True,
                timeout=2,
                universal_newlines=True)
        with open(str(tmp_path), 'r') as f:
            system = f.read()
        return return_management(system, out_format)


def add_dipole(system, n_m, out_format="ase", easy=True, shift_dislo1=(0, 0), shift_dislo2=(0, 0)):
    """Add a dislocations dipole to a cell created by `bilby.generate.create_bulk`.
    The two dislocations are of the opposite sign, and are either easy
    (default) or hard. Atoms positions are calculated using anisotropic
    theory of elasticity (ie. the Fortran package Babel by Emmanuel
    Clouet [1]_). The system is NOT RELAXED, and is only based on elasticity.

    Parameters
    ----------

    system : atomman.core.System
        An atomman system corresponding to pure BCC iron.
    n_m : tuple of int
        The values of n and m (see ref [3]_)
    out_format : str or list of str
        The type(s) of the output : string, ase.Atoms object, or atomman.System object.
    easy : boolean (default is True)
        The nature of the dislocation dipole. Easy by default.
    shift_dislo1 : tuple of int or float (default is (0,0))
        A shift of the core of the first dislocation, in direction (x, y).
        If not (0,0), the dislocations position will be shifted, without
        affecting the cell vectors that are calculated for the reference
        state that is no shift. It allow to work at constant volume, which
        is needed when doing Nudge Elastic Band in LAMMPS [2]_.
    shift_dislo2 : tuple of int or float
        A shift of the core of the second dislocation, in direction (x, y).
        Same remark concerning cell vectors.

    Returns
    ----------
    dipole : str or ase.Atoms or atomman.System
        An atomic system file that contains the dislocation dipole, NOT RELAXED.
        If `out_format = 'lammps'`, it's formatted as a lammps-readable string.
        If `out_format = 'ase'` , an ase.Atoms object is returned.
        If `out_format = 'atomman'` , an atomman.System object is
        returned.
        

    References
    ----------
    .. [1] Available at : http://emmanuel.clouet.free.fr/Programs/Babel/index.html
    .. [2] See https://lammps.sandia.gov/doc/neb.html
    .. [3] DEZERALD, Lucile. Modélisation ab-initio des dislocations dans les
        métaux de transition cubiques centrés. 2014. Thèse de doctorat. Grenoble.
    """
    if not isinstance(system, am.core.System):
        raise ValueError("System must be an atomman.core.System object")
    else:
        (n, m) = n_m

        # Save the current system, so that Babel will be able to read it...
        tmpdir = pathlib.Path('./tmp')
        tmpdir.mkdir(exist_ok=True)

        with open(tmpdir.joinpath("bulk.vasp"), "w") as f1:
            # Patch a bug in Babel's input reading with the "replace"
            f1.write(system.dump("poscar").replace("\nFe", "\n26"))

        # Read lattice parameter
        a_0 = system.box.vects[2][2]/(np.sqrt(3)/2.0)
        b = - np.array([0, 0, -np.sqrt(3)/2*a_0])
        if not easy:
            b[2] = - b[2]

        # Position of the first dislocation
        X1 = ((n+1)/4 + np.sqrt(6)*2 - np.sqrt(6)/6 + shift_dislo1[0]) * a_0
        Y1 = ((m+1)/4 + np.sqrt(2)*2 + np.sqrt(2)/6 + shift_dislo1[1]) * a_0

        # Position of the 2nd dislocation
        X2 = X1 + (n/2 + shift_dislo2[0]) * np.sqrt(6) / 3.0 * a_0
        Y2 = Y1 + (np.sqrt(2) / 6 + shift_dislo2[1]) * a_0

        # for var in (X1, Y1, X2, Y2):
        #     print("\n", var)

        # Calculate the new cell vectors, using ref. [3] p. 50
        q = 1/(3 * m)
        vects = np.asarray([[n, 0, -q],
                            [n/2, m, 1/2 - q/2],
                            [0, 0, 1]])
        vects[:, 0] = np.sqrt(6)*vects[:, 0] * a_0/3
        vects[:, 1] = np.sqrt(2)*vects[:, 1] * a_0/2
        vects[:, 2] = np.sqrt(3)*vects[:, 2] * a_0/2

        # Babel part
        with open(pathlib.Path('lib/templates/model_babel'), 'r') as f:
            filedata = f.read()
        filedata = filedata.replace(
            'number_of_atoms', '{}'.format(int(system.natoms)))
        filedata = filedata.replace(
            'input_file', '{}'.format(str(tmpdir.joinpath("bulk.vasp"))))
        filedata = filedata.replace('dislo1_x', '{}'.format(X1))
        filedata = filedata.replace('dislo1_y', '{}'.format(Y1))
        filedata = filedata.replace('dislo1_burg', '{}'.format(-b[2]))
        filedata = filedata.replace('dislo2_x', '{}'.format(X2))
        filedata = filedata.replace('dislo2_y', '{}'.format(Y2))
        filedata = filedata.replace('dislo2_burg', '{}'.format(
            b[2]))

        with open('tmp/input_babel', 'w') as f:
            f.write(filedata)

        # Run Babel to displace atoms. Logs are in tmp/babel.log
        subprocess.run(
            "babel/babel tmp/input_babel > tmp/babel.log",
            shell=True,
            timeout=5,
            universal_newlines=True
        )

        # Load Babel output
        with open("tmp/dislo.xyz", 'r+') as f:
            xyzFile = f.read().replace("\n26", "\nFe")  # Because Babel outputs a number instead of a symbol...
            f.seek(0)  # Put cursor at begining of the file to overwrite it
            f.write(xyzFile)
            f.truncate()  # Remove anything that would stand after the overwritten content
            dipole = ase.io.read(f, format='xyz')  # Atomman can't use xyz format as input...
            # and Babel's poscar output is buggy...

        # Do a permutation of box vects and atoms
        vects[[0, 1, 2], :] = vects[[2, 0, 1], :]
        vects[:, [0, 1, 2]] = vects[:, [2, 0, 1]]

        # Apply shear to the box
        positions = dipole.get_positions()
        positions[:, [0, 1, 2]] = positions[:, [2, 0, 1]]
        dipole.set_positions(positions)
        dipole.set_cell(vects)

        return return_management(dipole, out_format)


def PADbox(lattice, Nx, Ny, Nz, out_format="atomman", return_bulk=None):
    """DEPRECATED -- USE PADbox object instead. Left here for backward compatibility.
    Create PAD (Periodic Array of Dislocations) simulation box in BCC iron.
    Its orientation is x = [111], y = [-1-12], z = [1-10].
    It uses the command line tool Atomsk by Pierre Hiriel [1]_.
    It is NOT RELAXED, and naively created with respect to the
    lattice parameter passed as parameter.

    Parameters
    ----------

    lattice : float
        The target lattice parameter of the box to create.
    Nx : int
        Number of primitive cells along `x` axis.
    Ny : int
        Number of primitive cells along `y` axis.
    Ny : int
        Number of primitive cells along `z` axis.
    out_format : str or list of str, optional
        The format(s) of the system to return. Default is `atomman`.
    return_bulk : str, optional
        The format to use for returning a simulation box with no dislocation (eg. for differential
        displacements map).
        If set to `None` (default), only the PAD simulation box that contains a dislocation is returned.
        If set to "lammps", "ase" or "atomman", the function returns a dictionary with labels "bulk" and
        "PAD".

    Returns
    ----------
    system : dict
        A dictionary that contains every format name passed as `out_format` as key,
        and the PAD simulation box, in this/these format(s), as value(s).
        If `return_bulk` was specified, this dictionary is nested in another, that
        has two keys : `bulk` and `PAD`. This format will evolve (see issue tracker)

    References
    ----------
    .. [1] "Atomsk: A tool for manipulating and converting atomic data files"
        Pierre Hirel, Comput. Phys. Comm. 197 (2015) 212-219 | doi:10.1016/j.cpc.2015.07.012
    """
    print("Warning : generate.PADbox is deprecated -- USE bilby.pad.PADbox object instead.")
    
    with tempfile.TemporaryDirectory() as tmp:
        tmp_path = pathlib.Path(tmp).joinpath("PAD.lmp")

        atomsk_command = """atomsk --create bcc {lattice} Fe orient [111] [-1-12] [1-10] -duplicate {Nx} {Ny} {Nz} {tmp}""".format(
            lattice=lattice,
            Nx=Nx,
            Ny=Ny,
            Nz=Nz,
            tmp=tmp_path)
        # System call to Atomsk
        subprocess.run(
                atomsk_command,
                shell=True,
                check=True,
                timeout=2,
                universal_newlines=True)
        # Load system in atomman
        system = am.load('atom_data', str(tmp_path))

    initial_box = np.asarray(system.box.vects)  # copy box vectors
    Lx, Ly, Lz = system.box.a, system.box.b, system.box.c  # get lattice
    burgers = lattice*math.sqrt(3)/2  # get burgers

    if return_bulk is not None:
        bulk = deepcopy(system)
        bulk.box.set(vects=[initial_box[0],
                            initial_box[1],  # no tilt
                            initial_box[2] + [0, 0, Lz]])  # add vaccum

    system.box.set(vects=[initial_box[0],
                          initial_box[1] + [burgers/2, 0, 0],  # tilt yz plan
                          initial_box[2] + [0, 0, Lz]])  # add vaccum

    y_dislo = Ly/2-burgers/2 if Ny % 2 == Nz % 2 else Ly/2  # position of the dislocation core
    z_dislo = Lz/2
    # displacement field of dislocation
    system.atoms.pos = [[p[0] + burgers/2/np.pi*math.atan2(p[2]-z_dislo, p[1]-y_dislo),
                        p[1],
                        p[2]] for p in system.atoms.pos]

    if return_bulk is not None:
        return {"PAD": return_management(system, out_format),
                "bulk": return_management(bulk, return_bulk)}
    else:
        return return_management(system, out_format)


def ase2lmp(aseAtoms):
    """Converts an ASE atoms object to LAMMPS format.
    This feature was not implemented in ASE yet (as of Apr. 2019).

    Parameters
    ----------

    aseAtoms: ase.atoms.Atoms
        The ase Atoms object we want to convert to LAMMPS format.
        Its cell vectors must comply with LAMMPS's format [1]_, ie have
        a shape `cell = [[xx, 0, 0],[xy, yy, 0],[xz, yz, zz]]`.

    Returns
    ----------
    lmpAtoms : str
        A LAMMPS formatted string representing the input file.

    References
    ----------
    .. [1] https://lammps.sandia.gov/doc/Howto_triclinic.html
    """
    if not isinstance(ase.Atoms(), ase.atoms.Atoms):
        raise ValueError("System must be an ase.atoms.Atoms object")

    cell = aseAtoms.get_cell()
    if cell[0][1] != 0 or cell[0][2] != 0 or cell[1][2] != 0:
        raise ValueError("Cell vectors should comply with Lammps format.")
    
    # id of the atom in the positions list :
    indexes = range(1, aseAtoms.get_number_of_atoms()+1)
    # id of the atom type in type list
    types = [x for x in set(aseAtoms.get_atomic_numbers())]

    data = {
        "natoms": aseAtoms.get_number_of_atoms(),
        "ntypes": len(set(aseAtoms.get_chemical_symbols())),
        "atoms": " \n".join(["{} {} {}".format(
                            num,
                            types.index(attype)+1,
                            ' '.join(map(str, pos)))
                    for num, attype, pos in
                    zip(
                    indexes,
                    aseAtoms.get_atomic_numbers(),
                    aseAtoms.positions
                    )
                    ]),
        "xhi": cell[0][0],
        "yhi": cell[1][1],
        "zhi": cell[2][2],
        "xy": cell[1][0],
        "xz": cell[2][0],
        "yz": cell[2][1]
        }
        
    lmpAtoms = """# Written with Bilby https://gitlab.com/arnall-md/bilby
{natoms} atoms
{ntypes} atom types
0 {xhi} xlo xhi
0 {yhi} ylo yhi
0 {zhi} zlo zhi
{xy} {xz} {yz} xy xz yz

Atoms

{atoms}
    """.format(**data)
    return lmpAtoms


def return_management(system, out_format):
    """Transforms an atomic simulation system from a format
    to another.
    - Supported input formats are `"ase"`, `"atomman"` and `"lammps"`.
    - Supported outputs are `"ase"`, `"atomman"` and `"lammps"`.
    Input type is automatically detected using `type(system)`.

    Parameters
    ----------

    system: `ase.atoms.Atoms` or `atomman.system` or lammps-formated `str`
        The atoms system object we want to convert to `output_format`.
    out_format: `str` or `array of str`
        The type of the returned atoms system object. Can be an array of
        strings. Supported types associated with keywords are :
        - "ase" : ase.atoms.Atoms
        - "atomman" : atomman.system
        - "lammps" : lammps formated str

    Returns
    ----------
    system_output : dict
        A dictionary that contains every format name passed as `out_format`
        as key, and the input `system` in this format as value.
    
    Examples
    ----------
    >>> systems = return_management(system, ['ase', 'lammps'])
    >>> type(systems['ase'])
    ase.atoms.Atoms
    """
    in_type = str(type(system))

    if in_type == out_format:
        return system

    system_output = {}

    if in_type == "<class 'str'>":
        if 'lammps' in out_format:
            system_output['lammps'] = system
        if 'atomman' in out_format:
            system_output['atomman'] = am.load('atom_data', system, symbols=['Fe'])
        if 'ase' in out_format:
            system_output['ase'] = ase.io.read(system, format='lammps-data')
    elif in_type == "<class 'ase.atoms.Atoms'>":
        if 'lammps' in out_format:
            system_output['lammps'] = ase2lmp(system)
        if 'atomman' in out_format:
            system_output['atomman'] = am.load('ase_Atoms', system)
        if 'ase' in out_format:
            system_output['ase'] = system
    elif in_type == "<class 'atomman.core.System.System'>":
        if 'lammps' in out_format:
            system_output['lammps'] = system.dump('atom_data', return_info=False)
        if 'atomman' in out_format:
            system_output['atomman'] = system
        if 'ase' in out_format:
            system_output['ase'] = system.dump('ase_Atoms', system)
    else:
        raise ValueError("System type {} not supported.".format(in_type))
    
    if system_output == {}:
        raise ValueError(
            "Supported output formats are ['ase', 'lammps', 'atomman'], got {} instead".format(out_format))
    return system_output
