from __future__ import division, absolute_import, print_function
import os
import datetime
import numpy as np
from pdb import set_trace as st
import click
import json
import pathlib
import atomman as am  # https://github.com/usnistgov/atomman
import atomman.lammps as lmp
import atomman.unitconvert as uc
import iprPy  # https://github.com/usnistgov/iprPy
# https://github.com/usnistgov/DataModelDict
from DataModelDict import DataModelDict as DM
import pprint
import pandas as pd
import timeit
import subprocess

def main():
    """ This is the application made for comparing the Fe-C potentials available in
    the litterature in 2019. It uses the in-house EMU module.
    """

    # a dictionary containing the potentials, tests and systems filepaths.
    # It is also saved in YAML format in run/, with the date as file name.
    # md5sum of files is calculated, to detect files modified since last
    # execution of the application.
    files = emu.todo()

    # Generate simulation boxes based on the boxes.yml file
    emu.generate('boxes.yml')


    # Run the calculations for every loop / potential / ... 
    results = compute(files)
    extracted = extract(results)
    save(results, type = "rawdata")
    save(extracted, type = "extracted")




# def lastrun(extension="yml"):
#     """Find the logfile of the latest run."""
#     runs = [datetime.strptime(file, '%b%d%Y%I:%M%p.{}'.format(extension))
#             for file in os.listdir('./run/') 
#             if file.endswith(extension)])
#     last_run = pathlib.Path(max(runs))
#     return last_run







    if bool_calc:
        start = timeit.default_timer()
        results = computation(potentials, tests, systems, bool_vitek)
        stop = timeit.default_timer()
        print('Time: ', stop - start)
        results["conditions"] = {"date": datetime.datetime.now().strftime("%Y-%m-%d"),
                                 "lammps_version": lmp.checkversion('lmp_serial')['version'],
                                 "iprPy_version": iprPy.__version__,
                                 "atomman_version": am.__version__
                                 }
        results["units"] = {
            "length": "angstrom",
            "energy": "eV",
            "pressure": "GPa",
        }
        for entry, val in results['conditions'].items():
            print('{} {}'.format(entry, val))

        # Save results
        pathlib.Path("output").mkdir(exist_ok=True)
        with open('output/results.json', "w", newline='\r\n') as save_file:
            json.dump(results, save_file, indent=4)
            print("saved results in file results.json")

    if bool_extract:
        try:
            with open('output/results.json', "r") as f:
                results = json.load(f)
        except:
            print("JSON results file not found in location :\n{}".format(
                str(pathlib.Path.cwd().joinpath('output/results.json'))))
        else:
            df = extract_results.extract_from_json(results)
            df.to_csv("output/data_out.csv")

    if not bool_calc and not bool_extract:
        print("""No flags enabled : Nothing to do ! You should try "python3 main.py --help" Exiting...""")

    print("\nLogfiles that don't match stopping criterion : 'force tolerance' :\n ")
    subprocess.run(
        """find -name 'log*.lammps' | xargs grep -L 'force tolerance'| xargs grep 'criterion'""", shell=True)
    print("exit.")
